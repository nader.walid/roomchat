import 'package:RoomChat/Screens/ChatScreen.dart';
import 'package:flutter/material.dart';
import 'package:RoomChat/constant.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController _roomIdController = TextEditingController();
  TextEditingController _usernameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text('RoomChat'),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _roomIdController,
              textAlign: TextAlign.center,
              decoration: kInputDecoration.copyWith(hintText: 'Enter Room Id'),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: _usernameController,
              textAlign: TextAlign.center,
              decoration:
                  kInputDecoration.copyWith(hintText: 'Enter Your Name'),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: Material(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(30.0),
                elevation: 5.0,
                child: MaterialButton(
                  onPressed: () {
                    String username=_usernameController.text;
                    String roomid=_roomIdController.text;
                    _roomIdController.text="";
                    _usernameController.text="";
                    if (username != '' &&
                        roomid != '') {

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChatScreen(
                            username: username,
                            roomId: roomid,
                          ),
                        ),
                      );

                    }
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: Text(
                    "Join",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
